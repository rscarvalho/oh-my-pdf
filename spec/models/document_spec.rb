require 'spec_helper'

describe Document do
  it { should have_db_column(:contents).of_type(:text) }
  it { should have_db_column(:original_file).of_type(:string) }
  it { should have_db_column(:processed).of_type(:boolean).with_options(default: false) }
end
