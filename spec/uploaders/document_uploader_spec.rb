require 'spec_helper'
require 'fileutils'

describe DocumentUploader do
  include CarrierWave::Test::Matchers

  let(:document) { stub_model(Document, :id => 9998, :persisted? => true) }
  let(:uploader) { DocumentUploader.new(document, :original_file) }
  let(:pdf_article) { File.open(Rails.root.join('spec/fixtures/pdf/sample_input_pdf_article.pdf')) }
  let(:pdf_contract) { File.open(Rails.root.join('spec/fixtures/pdf/sample_input_pdf_contract.pdf')) }
  let(:pdf_ebook) { File.open(Rails.root.join('spec/fixtures/pdf/sample_input_pdf_ebook.pdf')) }
  let(:pdf_lyrics) { File.open(Rails.root.join('spec/fixtures/pdf/sample_input_pdf_lyrics.pdf')) }

  let(:txt_article) { File.open(Rails.root.join('spec/fixtures/pdf/sample_input_pdf_article.txt')).read }
  let(:txt_contract) { File.open(Rails.root.join('spec/fixtures/pdf/sample_input_pdf_contract.txt')).read }
  let(:txt_ebook) { File.open(Rails.root.join('spec/fixtures/pdf/sample_input_pdf_ebook.txt')).read }
  let(:txt_lyrics) { File.open(Rails.root.join('spec/fixtures/pdf/sample_input_pdf_lyrics.txt')).read }

  before do
    DocumentUploader.enable_processing = true
  end

  after do
    DocumentUploader.enable_processing = false
    uploader.remove!
  end

  it "stores the original pdf file in public/uploads/documents/9998 folder" do
    uploader.store!(pdf_contract)
    pdf_filename = File.basename(pdf_contract.path)

    expect("uploads/documents/9998/#{pdf_filename}").to eql(uploader.store_path(pdf_filename))
  end

  context "text extraction" do
    it "generates a text file with the same name as the filename but replacing the extension to '.txt'" do
      uploader.store!(pdf_contract)
      uploader.extract_text!
      pdf_filename = File.basename(pdf_contract.path)
      txt_name = pdf_filename.gsub(/\.[^.]+$/, '.txt')

      expect(File.exists?(Rails.root.join("public/uploads/documents/9998/#{txt_name}"))).to be_true
    end

    it "should match the contents of the file" do
      uploader.store!(pdf_contract)
      uploader.extract_text!
      pdf_filename = File.basename(pdf_contract.path)
      txt_name = pdf_filename.gsub(/\.[^.]+$/, '.txt')
      expect(File.read("public/uploads/documents/9998/#{txt_name}")).to eql(txt_contract)
    end
  end
end
