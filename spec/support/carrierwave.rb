CarrierWave.configure do |config|
  config.enable_processing = false
  config.storage = :file
end
