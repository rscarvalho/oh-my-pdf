www.laytons.com
8619882v1/SHARED - LAYTONS
We set out below an example of a shareholders’ agreement between three shareholders in a simple
private limited company. This is only an example agreement and should not be used as a template for
an agreement without consulting us first as it may be missing certain essential provisions.
THIS SHAREHOLDERS’ AGREEMENT is made on [ ] 2010
BETWEEN
(1) Mr. A of 22 St John Street, Manchester, M3 4EB (“Shareholder A”); and
(2) Mrs B of 22 St John Street, Manchester, M3 4EB (“Shareholder B”); and
(3) Miss C of 22 St John Street, Manchester, M3 4EB (“Shareholder C”)
WHEREAS
(A) Societas (UK) Limited (“Company”) is a private company limited by shares incorporated and
registered in England and Wales with company number 12345678 whose registered office is
at 22 St John Street, Manchester, M3 4EB. The Company has an authorised share capital of
£1,000, divided into 1,000 ordinary shares of each, all of which are issued and fully paid
(B) Each Shareholder is the registered owner of the following ordinary shares of £1 each in the
Company, for which each Shareholder has paid consideration at par value:
Mr A 250
Mrs B 250
Miss C 500
(C) The Shareholders have agreed to enter into this Agreement for the purpose of controlling their
capacity as shareholders of the Company
NOW IT IS AGREED that:
1. DEFINITIONS AND INTERPRETATION
1.1 DEFINITIONS
The definitions and rules of interpretation in this clause apply in this Agreement
“Auditors” the auditors of the Company from time to time;
“Business Day” a day (other than a Saturday or Sunday) when
banks in the City of London are open for
business; and
“Shareholder” any of Shareholders A, B and C as the context
requires, and “Shareholders” means all of them
together
1.2 Clause and Schedule Headings
Clause and schedule headings do not affect the interpretation of this Agreement
1.3 Reference to “Persons”
A “person” includes a natural person, a corporate or unincorporated body (whether or not
having a separate legal personality)
Shareholders’ agreement - Example
www.laytons.com
8619882v1/SHARED - LAYTONS
1.4 Reference to Laws
A reference to a particular law is a reference to it as it is in force from time to time taking
account of any amendment, extension, application or re-enactment, and includes any
subordinate legislation from time to time in force made under it
1.5 References to Documents “in Agreed Form”
Documents in “agreed form” are documents in the form agreed by the parties and initialled by
them for identification
1.6 References to the Singular and Plural
Words in the singular include the plural and in the plural include the singular
2. BUSINESS OF THE COMPANY
The Shareholders shall, for as long as they hold shares in the capital of the Company, procure
(so far as is possible in the exercise of their rights and powers) that the business of the
Company is carried on in accordance with the objects in the memorandum of association as
adopted by the Company for the time being
3. MATTERS REQUIRING CONSENT OF THE SHAREHOLDERS
The Shareholders shall, for as long as they hold shares in the capital of the Company, procure
(so far as is possible in the exercise of their rights and powers) that the Company shall not
without the prior written consent of all Shareholders:
• cease to be a private company or change the nature of its business from the type of
business conducted on or prior to the date of this Agreement
• amend its articles of association or the objects contained in its memorandum of
association
• change the name of the Company
• sell or otherwise dispose of the whole or any part of its undertaking, property, assets or
any subsidiary, or any interest therein or contract to do so whether or not for valuable
consideration
• alter any rights attaching to any class of share in the capital of the Company, or create
any option or right to acquire any shares in the capital of the Company
• conduct its business otherwise than in the ordinary course of business on an arm’s
length basis
• do, permit or suffer to be done any act or thing whereby the Company may be wound-
up, or enter into any compromise or arrangement under the Insolvency Act 1986
• merge or amalgamate with any other company or undertaking, or acquire directly or
indirectly any interest in any shares or other security convertible into shares of any
other company, or form or acquire any subsidiary
• purchase, lease or otherwise acquire assets or any interests therein which exceed the
value of £1,000
• enter into any contract, transaction or arrangement of a value exceeding £1,000
• effect any increase, reduction, sub-division, cancellation, purchase or redemption of
the capital or any allotment or issue of shares of the Company
• borrow any money in excess of any limits agreed between the Shareholders, or create
any mortgage, debenture, pledge, lien or other encumbrances over the undertaking or
assets of the Company, or factor, assign, discount or otherwise dispose of any book
debts or other debts of the Company
• give any guarantee, make any payment or incur any obligation or act as surety
otherwise than in connection with the Company’s ordinary business for the time being
• lend or agree to lend, grant any credit or make any advance to any person otherwise
than in the ordinary course of the business of the Company
• remove any director appointed by a Shareholder
• pass any resolution or engage in any other matter which represents a substantial
change in the nature of the business of the Company or in the manner in which the
business is conducted
www.laytons.com
8619882v1/SHARED - LAYTONS
• hold any meeting of the shareholders or purport to transact any business at such
meeting, unless authorised representatives or proxies are present for each of the
Shareholders
4. DIRECTORSHIPS
If a Shareholder sells or disposes of all or part of his shares in the Company so that the
aggregate nominal value of his holding of shares falls below 25% in nominal value of the
issued ordinary share capital of the Company, he shall immediately resign any office and
employment with the Company without claim for compensation
5. TERMINATION
5.1 Termination Events
This Agreement terminates immediately upon the occurrence of any of the following events:
• a resolution is passed for the winding up of the Company
• a receiver, administrator or administrative receiver is appointed over the whole or any
part of the assets of the Company or the affairs, business and property of the
Company is to be managed by a supervisor under any arrangement made with the
creditors of the Company.
5.2 Prior Rights
Termination of this Agreement shall be without prejudice to the rights of any Shareholder
accrued prior to such termination, or under any provision which is expressly stated not to be
affected by such termination including in respect of any prior breach of this Agreement
5.3 Consequences of Termination
On a winding-up, the Shareholders shall endeavour to agree a suitable basis for dealing with
the interests and assets of the Company and shall endeavour to ensure that:
• all existing contracts of the Company are performed so far as resources permit
• no new contractual obligations are entered into by the Company
• the Company is wound up as soon as practicable
6. STATUS OF THIS AGREEMENT AND THE PARTIES’ OBLIGATIONS
6.1 Parties’ Obligations
Each Shareholder shall exercise all voting rights and other powers of control available to him
in relation to the Company so as to procure (so far as each is respectively able by the exercise
of such rights and powers) that, at all times during the term of this Agreement, the provisions
of this Agreement are duly and promptly observed and given full force and effect according to
its spirit and intention
6.2 Status of this Agreement
If any provisions of the memorandum or articles of association of the Company at any time
conflict with any provisions of this Agreement, this Agreement shall prevail and the
Shareholders shall, whenever necessary, exercise all voting and other rights and powers
available to them to procure the amendment, waiver or suspension of the relevant provision of
the memorandum or articles of association to the extent necessary to permit the Company
and its affairs to be administered as provided in this Agreement
7. NO PARTNERSHIP
The Shareholders are not in partnership with each other, nor are they agents of each other
8. CONFIDENTIALITY
Each Shareholder undertakes that he shall not at any time after the date of this Agreement
use, divulge or communicate to any person (except to his professional representatives or
advisers or as may be required by law or any legal or regulatory authority) any confidential
information concerning the terms of this Agreement, the business or affairs of the other
Shareholders or the Company which may have (or may in future) come to his knowledge, and
www.laytons.com
8619882v1/SHARED - LAYTONS
each of the Shareholders shall use his reasonable endeavours to prevent the publication or
disclosure of any confidential information concerning such matters
9. NOTICES
9.1 Notice
Any notice given under this Agreement shall be in writing and shall be delivered by hand,
transmitted by fax, or sent by pre-paid first class post or recorded delivery post to the address
of the party as set out in clause 9.2, or to such other address notified to the other parties. A
notice delivered by hand is deemed to have been received when delivered (or if delivery is not
in business hours, 9.00 am on the first Business Day following delivery). A correctly addressed
notice sent by pre-paid first class post or recorded delivery post shall be deemed to have been
received at the time at which it would have been delivered in the normal course of post. A
notice sent by fax to the fax number of the relevant party shall be deemed to have been
received at the time of transmission
9.2 Addresses for Service of Notices
The addresses for service of notice are:
Shareholder A
Address: 22 St John Street
Fax number: 0161 834 6862
For the attention of: Mr A
Shareholder B
Address: 22 St John Street
Fax number: 0161 834 6862
For the attention of: Mrs B
Shareholder C
Address: 22 St John Street
Fax number: 0161 834 6862
For the attention of: Miss C
10. SEVERANCE
10.1 Severance (1)
If any provision (or part of a provision) of this Agreement is found by any court or
administrative body of competent jurisdiction to be invalid, unenforceable or illegal, the other
provisions shall remain in force
10.2 Severance (2)
If any invalid, unenforceable or illegal provision would be valid, enforceable and legal if some
part of it were deleted, the provision shall apply with whatever modification is necessary to
give effect to the commercial intention of the parties
11. COSTS AND EXPENSES
Each Shareholder shall pay the costs relating to the negotiation, preparation, execution and
implementation by him of this Agreement in the same proportion to which he holds shares in
the Company.
12. GOVERNING LAW AND JURISDICTION
12.1 Governing Law
This Agreement and any disputes or claims arising out of or in connection with its subject
matter are governed by and construed in accordance with the laws of England
12.2 Jurisdiction
The parties irrevocably agree that the courts of England have exclusive jurisdiction to settle
any dispute or claim that arises out of or in connection with this Agreement
www.laytons.com
8619882v1/SHARED - LAYTONS
AS WITNESS the hands of the parties or their duly authorised representatives the date first above
written
Signed by Mr A ....................
Signed by Mrs B ....................
Signed by Miss C ....................
Author: Paul Caddy (paul.caddy@laytons.com)
March 2010
