class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :original_file
      t.text :contents
      t.boolean :processed, :default => false

      t.timestamps
    end

    add_index :documents, :created_at
  end
end
