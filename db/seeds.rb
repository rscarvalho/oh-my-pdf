require 'fileutils'

Document.destroy_all
FileUtils.rm_rf("public/uploads/documents")

Dir["spec/fixtures/pdf/*.pdf"].each do |f|
  puts f
  file = File.open(f)
  Document.create!(original_file: file)
end
