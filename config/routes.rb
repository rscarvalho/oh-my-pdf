OhMyPdf::Application.routes.draw do
  namespace :api, defaults: {format: 'json'} do
    resources :documents
  end
  root to: 'home#index'
end
