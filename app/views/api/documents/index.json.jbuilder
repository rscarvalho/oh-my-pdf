json.documents collection, partial: 'item', as: :resource

json.meta do
  json.original_url api_document_url(':id:', format: 'pdf')
  json.txt_url api_document_url(':id:', format: 'txt')
  json.html_url api_document_url(':id:', format: 'html')
end
