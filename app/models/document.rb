class Document < ActiveRecord::Base
  mount_uploader :original_file, DocumentUploader
  after_save :extract_text

  def process!
    update_attribute(:processed, true)
  end

  def txt_file_url
    original_file.url.gsub(/\.[^.]+$/, '.txt')
  end

  protected
  def extract_text
    return if processed?
    self.contents = original_file.extract_text!
    self.processed = true
    save
  end
end

