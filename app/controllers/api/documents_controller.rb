class Api::DocumentsController < ApplicationController
  respond_to :json
  respond_to :pdf, :text, :html, :only => :show
  inherit_resources

  skip_before_filter :verify_authenticity_token

  def show
    respond_with(resource) do |format|
      format.pdf { redirect_to resource.original_file.url }
      format.text { redirect_to resource.txt_file_url }
      format.html { render :text => resource.contents, :content_type => 'text/html' }
    end
  end

  protected
  def permitted_params
    params.permit(:document => [:original_file])
  end
end
