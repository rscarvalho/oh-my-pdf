@App.Document = DS.Model.extend
  filename: DS.attr('string')
  createdAt: DS.attr('date')
  updatedAt: DS.attr('date')
  processed: DS.attr('boolean')
