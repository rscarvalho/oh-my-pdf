@App.DocumentsIndexRoute = Ember.Route.extend
  model: ->
    @store.findAll('document')

  actions:
    dropzoneComplete: ->
      @set('model', @store.findAll('document'))
      @transitionToRoute('document.index')
