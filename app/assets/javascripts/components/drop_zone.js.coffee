@App.DropZoneComponent = Ember.Component.extend
  tagName: 'span'
  didInsertElement: ->
    self = this
    this.$().find("form").first().dropzone
      paramName: @get('paramName') || 'file'
      acceptedFiles: "application/pdf"

      init: ->
        @on 'complete', ->
          Ember.run.next ->
            self.sendAction('dropzoneComplete')
