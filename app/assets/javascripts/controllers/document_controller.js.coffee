@App.DocumentController = Ember.ObjectController.extend
  formatUrl: (url, args) ->
    final = url
    for key, value of args
      final = url.replace(new RegExp(":#{key}:", 'g'), value)
    final

  urlForFormat: (format) ->
    url = switch format
      when 'pdf'
        'original_url'
      when 'txt'
        'txt_url'
      when 'html'
        'html_url'
    id = @get('model').get('id')
    metadata = @store.metadataFor('document')
    @formatUrl(metadata[url], id: id)


  actions:
    downloadPdf: ->
      window.location.href = @urlForFormat('pdf')
    downloadTxt: ->
      window.location.href = @urlForFormat('txt')
    downloadHtml: ->
      window.location.href = @urlForFormat('html')
