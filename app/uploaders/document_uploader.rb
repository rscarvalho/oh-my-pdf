# encoding: utf-8

class DocumentUploader < CarrierWave::Uploader::Base
  # process :generate_text
  attr_writer :being_generated
  def being_generated?
    !!@being_generated
  end

  def store_dir
    "uploads/#{model.class.model_name.plural.underscore}/#{model.id}"
  end

  def extension_white_list
    %w(pdf txt)
  end

  def extract_text!
    return if being_generated?

    txt_filename = File.basename(@file.path).gsub(/\.[^.]+$/, '.txt')
    pdftotext!(@file.path, nil)
    out = File.read("#{File.dirname(@file.path)}/#{txt_filename}")
    io = StringIO.new(out)
    f = CarrierWave::SanitizedFile.new(:tempfile => io, :filename => txt_filename, :content_type => 'text/plain')
    uploader = DocumentUploader.new(model, mounted_as)
    uploader.being_generated = true
    uploader.store!(f)
    return out
  end


  def pdftotext!(from, to)
    cmd = ENV['PDFTOTEXT_BIN'] || '/usr/local/bin/pdftotext'
    cmd_args = ['-raw', '-eol unix', '-enc UTF-8', '-nopgbrk', '-layout']

    cmd_args += [from.shellescape]
    %x|#{cmd} #{cmd_args.join(' ')}|
  end


  class TextGenerationError < Exception
  end
end
